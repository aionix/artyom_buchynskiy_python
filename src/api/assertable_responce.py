import requests


class AssertableResponse(object):

    def __init__(self, response):
        self.response = response

    def should_have_status_code(self, status_code):
        assert self.response.status_code == status_code

    def should_have_body_field(self, name, matcher):
        if matcher == 'id':
            assert len(self.response.json()[name]) < 0
        else:
            assert  self.response.json()[name] == name



