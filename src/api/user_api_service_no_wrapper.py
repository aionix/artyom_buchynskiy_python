import json
import requests


class UserApiServiceNoWrapper():
    def __init__(self):
        self.base_url = "http://104.248.133.198"

    def create_user(self, json_data):
        _json_data = json.dumps(json_data)
        return requests.post(url=self.base_url + '/register', data=_json_data, headers={'Content-Type': 'application/json'})

    def get_user_by_id(self, customer_id):
        _url = self.base_url + "/customers/{}".format(customer_id)
        return requests.get(_url)
