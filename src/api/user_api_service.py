import json
import requests

from src.api.assertable_responce import AssertableResponse


class UserApiService():
    def __init__(self,  *args, **kwargs):
        self.base_url = "http://104.248.133.198:8080"
        self.request_cookie = {}

    def create_user(self, json_data):
        _json_data = json.dumps(json_data)
        return AssertableResponse(requests.post(url=self.base_url + '/register', data=_json_data, headers={'Content-Type': 'application/json'}))

    def get_user_by_id(self, customer_id):
        _url = self.base_url + "/customers/{}".format(customer_id)
        return requests.get(_url)

    def login_user(self):
        response = requests.get(url=self.base_url + "/login", auth=('asd7', 'asd7'))
        self.request_cookie['cookies'] = response.cookies
        return response

    def get_users(self):
        return requests.get(url=self.base_url + "/customers", **self.request_cookie)

    #not ready method yet
    def get_catalogue(self, cookie):
        return requests.get(url=self.base_url + "/catalogue?size=1", **self.request_cookie)

    def get_cards(self):
        return requests.get(url=self.base_url + "/cards", **self.request_cookie)

    def create_card(self, card_data):
        _card_data = json.dumps(card_data)
        return requests.post(url=self.base_url + "/cards", data=_card_data, **self.request_cookie)


