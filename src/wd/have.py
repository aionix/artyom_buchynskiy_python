from src.wd.conditions import CollectionSizeCondition, ElementTextCondition


def size(size):
    return CollectionSizeCondition(size)

def text(text):
    return ElementTextCondition(text)
