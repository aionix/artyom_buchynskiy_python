from abc import abstractmethod

from src.wd.exceptions import ConditionMismatchException


class CollectionCondition(object):
     def __init__(self):
         pass

     @abstractmethod
     def match(self, elements):
         pass

class ElementCondition(object):
    def __init__(self):

     @abstractmethod
     def match(self, elements):
         pass

class CollectionSizeCondition(CollectionCondition):
    def __init__(self, size):
        super().__init__()
        self.__size = size

    def match(self, elements):
        return elements.size() == self.__size

class ElementTextCondition(ElementCondition):
    def match(self, element):
        if(self._text == element.text):
            return True
        raise ConditionMismatchException("Wrong text")

    def __init__(self, text):
        super().__init__()
        self._text = text

