from selenium.webdriver.common.keys import Keys

from src.wd.waiter import wait_for


class UIElement(object):

    def __init__(self, driver, locator):
        self.__driver = driver
        self.__locator = locator


    def get_wrapped_element(self):
        return self.__driver.find_element_by_css_selector(self.__locator)

    def click(self):
        self.get_wrapped_element().click()
        return self

    def set_value(self, value):
        self.get_wrapped_element().send_keys(value)
        return self

    def press_enter(self):
        self.get_wrapped_element().send_keys(Keys.ENTER)

    def should(self, condition):
        wait_for(self, condition)

    @property
    def text(self):
        return self.get_wrapped_element().text


class UIElementCollection(object):
    def __init__(self, driver, locator):
        self.__driver = driver
        self.__locator = locator

    def get_wrapped_elements(self):
        return self.__driver.find_elements_by_css_selector(self.__locator)

    def size(self):
        return len(self.get_wrapped_elements())

    def should(self, condition):
        wait_for(self, condition)

    def first(self):
        return  UIElement(self.__driver, self.__locator)
