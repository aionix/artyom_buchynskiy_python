import time


def wait_for(element, condition, timeout=4, polling=0.5):
    end_time = time.time() + timeout
    while True:
        try:
            return condition.match(element)
        except Exception as ex:
            if time.time() > end_time:
                raise ex
            time.sleep(polling)
