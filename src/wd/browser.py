from selenium.webdriver.chrome.webdriver import WebDriver

from src.wd.element import UIElement, UIElementCollection


class Browser():
    def __init__(self, driver):
        # type: (WebDriver)->()
        self.__driver = driver

    def visit(self, url):
        self.__driver.get(url)

    def element(self, css_selector):
        return UIElement(self.__driver, css_selector)

    def all(self, css_selector):
        return UIElementCollection(self.__driver, css_selector)


