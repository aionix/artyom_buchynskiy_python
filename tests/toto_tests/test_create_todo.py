from src.api.num_generator import get_cur_time
from src.api.to_do_service import ToDoService


def test_create_todo():
    #prepare data
    headers = ToDoService().login_user()
    data = {"content": "test todo_" + get_cur_time()}
    #creation of to_do and getting its id
    response = ToDoService().add_todo(data, headers)
    assert response.status_code == 200
    id = response.json()['id']

    todo = ToDoService().get_todo_by_id(str(id), headers)

    assert response.status_code == 200
    assert  data['content'] == todo['content']


def test_remove_todo():
    #preconditions - creating to_do which will be removed
    headers = ToDoService().login_user()
    data = {"content": "test todo_" + get_cur_time()}
    response = ToDoService().add_todo(data, headers)
    id = response.json()['id']
    #getting sum of to_dos before deletion
    number_of_todos_before = len(ToDoService().get_todos(headers).json())

    response = ToDoService().remove_todo_by_id(id, headers)
    assert response.status_code == 204

    number_of_todos_after = len(ToDoService().get_todos(headers).json())
    assert number_of_todos_before -1 == number_of_todos_after

