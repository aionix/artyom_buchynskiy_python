from src.api.num_generator import get_cur_time
from src.api.user_api_service_no_wrapper import UserApiServiceNoWrapper


def test_create_user():
    #prepare data
    username = "demo"+ get_cur_time()
    data = {"username": "" +username+ "", "password":"12345", "email":"1demo@gmail.com"}
    #get response
    response = UserApiServiceNoWrapper().create_user(data)


    assert response.status_code == 200
    assert len(response.json()['id']) > 0
    print(response.json()['id'])

def test_get_user_by_id():
    #prepare data and post
    username = "demo"+ get_cur_time()
    data = {"username": "" +username+ "", "password":"12345", "email":"1demo@gmail.com"}
    response = UserApiServiceNoWrapper().create_user(data)
    #get response
    id = response.json()['id']
    temp = UserApiServiceNoWrapper().get_user_by_id("5bcdedf7ee11cb0001d4c974")
    print(temp.content)

    # assert response.status_code == 200
    # assert response.json()['firstName'] == data['username']


