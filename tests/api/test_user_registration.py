from src.api.num_generator import get_cur_time
from src.api.user_api_service import UserApiService


def test_create_user():
    #prepare data
    username = "demo"+ get_cur_time()
    data = {"username": "" +username+ "", "password":"12345", "email":"1demo@gmail.com"}
    #get response
    response = UserApiService().create_user(data)

    response.should_have_status_code(200)
    response.should_have_body_field('id', 'id')


def test_get_user_by_id():
    #prepare data and post
    username = "demo"+ get_cur_time()
    data = {"username": "" +username+ "", "password":"12345", "email":"1demo@gmail.com"}
    id = UserApiService().create_user(data)['id']
    #get response
    response = UserApiService().get_user_by_id(id)

    #assert response.status_code == 200
    response.should_have_body_field('username')

    assert response.json()['username'] == data['username']


