import allure

from src.api.num_generator import get_cur_time


@allure.step
def test_mock_negative():
    print("Running NAGATIVE mock test")
    #assert False

@allure.step
def test_mock_positive():
    print("Running POSITIVE mock test")
    username = "demo__ "+ get_cur_time()
    print(username)
    assert True

def test_mock_positive_1():
    print("Running POSITIVE mock test")
    assert True
