from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from src.wd import have
from src.wd.browser import Browser


# def test_simple_webdriver():
#     chrome = webdriver.Chrome(executable_path=ChromeDriverManager().install())
#     chrome.get("http://todomvc.com/examples/vanillajs/")
#     input = chrome.find_element_by_css_selector(".new-todo")
#     input.send_keys("demo")
#     input.send_keys(Keys.ENTER)
#
#     items = chrome.find_elements_by_css_selector(".todo-list li label")
#     assert len(items) == 1
#     time.sleep(10)

def test_selenium_wrapper():
    browser = Browser(webdriver.Chrome(executable_path=ChromeDriverManager().install()))
    browser.visit("http://google.com")
    browser.element(".new-todo").set_value("demo").press_enter()
    items = browser.all(".todo-list li label")

    assert items.size() == 1
    items.should(have.size(1))
    assert items.first().should(have.text("demo"))
